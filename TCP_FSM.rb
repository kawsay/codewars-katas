def traverse_TCP_states(event_list)
  state = "CLOSED"  # initial state, always
  
  event_list.length.times do
    case event_list[0]
    when "APP_PASSIVE_OPEN"
      state == "CLOSED" ? state = "LISTEN"   : state = "ERROR"
    
    when "APP_ACTIVE_OPEN"
      state = "SYN_SENT"
        
    when "APP_SEND"
      state = "SYN_SENT"
    
    when "RCV_SYN_ACK"
      state = "ESTABLISHED"
    
    when "RCV_FIN_ACK"
      state = "TIME_WAIT"
    
    when "APP_TIMEOUT"
      state = "CLOSED"
    
    when "RCV_SYN"
      case state
        when "LISTEN"       then state = "SYN_RCVD"
        when "SYN_SENT"     then state = "SYN_RCVD"
        else                     state = "ERROR"
      end
      
    when "APP_CLOSE"
      case state
        when "LISTEN"       then state = "CLOSED"
        when "SYN_RCVD"     then state = "FIN_WAIT_1"
        when "SYN_SENT"     then state = "CLOSED"
        when "ESTABLISHED"  then state = "FIN_WAIT_1"
        when "CLOSE_WAIT"   then state = "LAST_ACK"
        else                     state = "ERROR"
      end
    
    when "RCV_ACK"
      case state
        when "SYN_RCVD"     then state = "ESTABLISHED"
        when "FIN_WAIT_1"   then state = "FIN_WAIT_2"
        when "CLOSING"      then state = "TIME_WAIT"
        when "LAST_ACK"     then state = "CLOSED"
        else                     state = "ERROR"
      end
  
    when "RCV_FIN"
      case state
        when "ESTABLISHED"   then state = "CLOSE_WAIT"
        when "FIN_WAIT_1"    then state = "CLOSING"
        when "FIN_WAIT_2"    then state = "TIME_WAIT"
        else                      state = "ERROR"
      end
      
    end
    event_list.shift
    
  end
  return state
  
end


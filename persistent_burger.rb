def persistence(n)
  persistance = 0
  
  while n >= 10 
    n = n.to_s.chars.map(&:to_i).inject(:*)
    persistance += 1
  end
  
  return persistance
end


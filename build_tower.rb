def towerBuilder(n)
  tower = []
  
  1.upto(n) {|floor| tower << floorBuilder(floor, n) }
  
  tower
end

def floorBuilder(floor, n)
  (' ' * (n - floor)) + ('*' * floor) + ('*' * (floor -1)) + (' ' * (n - floor))
end


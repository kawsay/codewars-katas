def wave(str)
  result = []
  str.chars.length.times{ |n| str[n] = str[n].upcase; str[n] =~ /[[:alpha:]]+/ ? result << letters_up(str, n)  : nil }
  result
end

def letters_up(str, n)
  output    = str.dup.downcase
  output[n] = output[n].upcase
  output
end


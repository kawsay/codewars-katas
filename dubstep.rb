str = "AWUBWUBBWUBWUBC"

def song_decoder(str)
    i = 0
    str = str.gsub("WUB", " ")
    while str[0] == " "
        str[0] = ""
    end

    while str[-1] == " "
        str[-1] = ""
    end

    while (str.match?("  "))
        str = str.gsub("  ", " ")
    end
    return str
end

song_decoder(str)
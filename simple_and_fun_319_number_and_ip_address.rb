def number_and_ipaddress(s)
  s.include?('.') ? to_32_bit_int(s) : to_ip(s)
end

def to_32_bit_int(s)
  s.split('.').map{|int| int.to_i.to_s(2).rjust(8, '0')}.join.to_i(2).to_s
end

def to_ip(s)
  s.to_i.to_s(2).rjust(32, '0').scan(/.{8}/).map{|byte| byte.to_i(2)}.join('.')
end


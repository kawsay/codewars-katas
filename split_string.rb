def solution(str)
  output = str.scan(/.{2}/)
  
  str.length % 2 != 0 ? output << (str[-1] + "_") : output
end

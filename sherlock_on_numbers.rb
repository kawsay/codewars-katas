# return suspect number from given set, return nil if no suspect found
def find_suspect(*numbers)
  count_i = numbers.inject(Hash.new(0)){|sum, n| sum[n] += 1; sum}
  
  suspect = count_i.min_by(&:last)
  
  return suspect[0] if count_i.select {|k, v| v == suspect[1]}.length ==  1
end


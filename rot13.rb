def rot13_char(character)
  if character.scan(/[a-mA-M]/).one?
    character.gsub!(/./) {|c| (c.ord + 13).chr}
  elsif character.scan(/[n-zN-Z]/)
    character.gsub!(/./) {|c| (c.ord - 13).chr}
  end
end

def rot13(string)
  string.chars.each {|c| c.match(/[a-zA-Z]/) ? rot13_char(c) : c }.join
end

puts rot13('wow lol')



# Best practices

# def rot13(string)
#   string.tr("A-Za-z", "N-ZA-Mn-za-m")
# end
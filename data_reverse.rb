def data_reverse(data)
  data.join.scan(/.{8}/).reverse.join.chars.map(&:to_i)
end


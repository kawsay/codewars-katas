def DNA_strand(dna)
    dna_complementary = {
    A: "T",
    T: "A",
    C: "G",
    G: "C"
}
    dna_completed = []
    dna.each_char do |c|
        if (c == dna_complementary.keys[0].to_s)
            dna_completed << dna_complementary[:A]
        elsif (c == dna_complementary.keys[1].to_s)
            dna_completed << dna_complementary[:T]
        elsif (c == dna_complementary.keys[2].to_s)
            dna_completed << dna_complementary[:C]
        elsif (c == dna_complementary.keys[3].to_s)
            dna_completed << dna_complementary[:G]
        end
    end
    puts dna_completed.join
    return dna_completed.join
end

dna = "ATTGAGTGGAGGTGACCCTGGACCT"
DNA_strand(dna)
def snail(arr)
  output = []
  return output if arr[0].size == 0
  
  0.upto(arr.size/2 - 1) do |i|
    output << arr[i][i..-1-i]
    (i + 1).upto(arr.size - i - 1){ |j| output << arr[j][-i - 1] }
    output << arr[-i - 1][i + 1..-i - 2].reverse
    (arr.length - i - 1).downto(i + 1){ |k| output << arr[k][i] }
  end
  
  output << arr[arr.size / 2][arr.size / 2] if arr.size.odd?
  
  output.flatten!
  output
end

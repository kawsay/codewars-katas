def unique_in_order(iterable)
  uniq_in_order = []
  
  iterable = iterable.chars if iterable.is_a?(String)
  
  iterable.map.with_index { |c, i| uniq_in_order << c if c != iterable[i+1] }
  
  uniq_in_order
end


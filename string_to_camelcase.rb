def to_camel_case(str)
  return '' if str.empty?
  
  str.gsub!("_", '-')
  
  camel_str = str.split('-').map{|word| word.capitalize}.join
  camel_str[0] = str[0]
  camel_str
  
end


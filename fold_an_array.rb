def fold_array(array, runs)
  runs.times do
  fold_array = []
    
    1.upto(array.size / 2) do |i|
      fold_array << array[i - 1] + array[-1 * i]
    end
    
    fold_array << array[array.size / 2] if array.size.odd?
    
    array = fold_array
  end
  array
end 


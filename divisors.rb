def divisors(n)
    all_divisors = []
    (1..n).each do |d|
        if n % d == 0
              all_divisors << d
        end
    end
    # print all_divisors
    # all_divisors.delete(1)
    # all_divisors.delete(n)
    # print all_divisors

    all_divisors.each do |value|
        # puts "value : #{value} for #{n}"
        if (value == 1)
            all_divisors.delete(1)
        end
        if (value % n == 1)
            all_divisors.delete(n)
        end
    end
    # print all_divisors
    if all_divisors.empty?
        puts "#{n} is prime"
    else
        puts all_divisors
    end
end

n = 13
divisors(n)
require 'pry'

def break_caesar(s)
  # Generate decrypted string each iteration, then check if the
  #   Intersection between uniq words in the decrypted string & WORDS
  #   reach a given treshold (0.7, hardcoded)
  # Return `i`(INT), the most likely shift value
  i = 0
  while i <= 25
    decrypt_string = reverse_caesar(s.dup, i)
    decrypt_words  = decrypt_string.downcase.scan(/\w+/).uniq
    matching_words = decrypt_words & WORDS.to_a
    return i if enough_match?(matching_words, decrypt_string)
    i += 1
  end
end

def enough_match?(arr, s)
  # Compare the lenght of both elements
  # `arr` is the Intersection between `decrypt_string` and `WORDS`
  s = s.downcase.scan(/\w+/).uniq
  arr.length > (s.length * 0.7)
end

def reverse_caesar(s, n)
  s.chars.map do |c|
    if letter_overflow?(c, n) 
      loop_and_shift_letter(c, n)
    elsif (c.between?('A', 'Z') && (c.ord - n) > 65) || (c.between?('a', 'z') && (c.ord - n) >= 97)
      shift_letter(c, n)
    else
      c
    end
  end.join
end

def letter_overflow?(c, n)
  c.between?('A', 'Z') && (c.ord - n) < 65 || c.between?('a', 'z') && (c.ord - n) < 97
end

def loop_and_shift_letter(c, n)
  (c.ord + 26 - n).chr
end

def shift_letter(c, n)
  (c.ord - n).chr
end



caesar('abc', 2)
binding.pry


# Best practice

# def break_caesar(message)
#   # sanitize the input
#   message.downcase!.gsub!(/[^a-z]+/, ' ').strip!
  
#   # try all possible shifts
#   hits = []
  
#   (0..25).each { |shift|
#     decoded = message.tr("a-z", ("a".."z").to_a.rotate(shift).join)
#     hits << decoded.split.count { |word| WORDS.include?(word) }
#   }
  
#   # find the most likely shift value
#   shift = (26 - hits.index(hits.max)) % 26
# end
def order(words)
  output = []
  
  10.times { |i| output << words.match(/\b\w*#{i}\w*\b/).to_s }
  
  output.join(' ').strip
end


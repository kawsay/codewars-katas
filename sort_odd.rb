def sort_array(arr)
  sorted_arr = arr.map{|e| e.odd? ? e : nil}.compact.sort
 
  arr.each_with_index {|e, i| sorted_arr.insert(i, e) if e.even? }
  
  return sorted_arr
end

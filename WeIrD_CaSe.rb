def weirdcase string
  string.split.map do |word|
    word.chars.each_with_index.map{ |c, i| i.even? ? c.upcase : c.downcase }.join  
  end.join(' ')
end


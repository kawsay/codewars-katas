def find_it(seq)
  seq.reduce(Hash.new(0)) { |s, e| s[e] += 1; s }.select{ |k, v| (v % 2 != 0) }.keys[0]
end


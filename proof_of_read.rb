def proofread(string)
  string.downcase.gsub('ie', 'ei').split('. ').map(&:capitalize).join('. ')
end


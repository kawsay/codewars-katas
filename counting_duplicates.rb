def duplicate_count(str)
  str.downcase!
  str.chars.inject(Hash.new(0)) { |sum, c| sum[c] += 1; sum }
     .select { |k, v| v > 1 }.length
end


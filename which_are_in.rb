def in_array(array1, array2)
  output = []
  
  array1.each do |word1|
    array2.each {|word2| output << word1 if word2.include?(word1) }
  end
  
  output.uniq.sort
end


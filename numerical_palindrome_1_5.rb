def palindrome(num,s)
  output = []
  
  return "Not valid" if num.is_a?(String) || s.is_a?(String) || num < 0 || s < 0
  
  while output.length < s
    output << num if num > 9 && num.to_s == num.to_s.reverse
    num += 1
  end
  
  output
end
  

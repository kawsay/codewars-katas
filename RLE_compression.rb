def encode(s)
  i = 0
  encoded_str = [[]]
  while i < s.length
    if s[i] == encoded_str[-1][-1]
      rle_str[-1][0] += 1      
    else
      encoded_str << [1, s[i]]
    end
    i += 1
  end
  encoded_str.join
end

def decode(s)
  i = 0
  decoded_str = ''

  while i < s.length
    n = /^[0-9]+/.match(s).to_s.to_i
    n_length = /^[0-9]+/.match(s).to_s.size
    s = s[n_length..-1]
    n.times do
      decoded_str << s[0]
    end
    s = s[1..-1]
  end
  decoded_str
end

# encode('AABZZZERR')
decode('2A13C4O')

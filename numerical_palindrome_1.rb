def palindrome(num)
  return "Not valid" if num.is_a?(String) || num < 0 
  num.to_s === num.to_s.reverse ? true : false
end


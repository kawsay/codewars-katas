def digital_root(n)
  root = n.to_s.chars.map(&:to_i).inject(:+)
  
  return root if root < 10
  
  digital_root(root)
end


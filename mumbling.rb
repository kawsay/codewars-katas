def accum(s)
	s.chars.each_with_index.map { |c, i| (c * (i + 1)).downcase.capitalize }.join('-')
end


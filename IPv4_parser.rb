def ipv4__parser(ip_addr, mask)
  ip_addr, mask   = to_ip(ip_addr), to_ip(mask)
  network_addr    = network_addr(ip_addr, mask)
  host_identifier = host_identifier(network_addr, ip_addr)
  
  return [network_addr.join('.'), host_identifier.join('.')]
end

def to_ip(str)
  str.split('.').map{|b|b.to_i}
end

def network_addr(ip, mask)
  ip.each_with_index.map{|byte, i| byte & mask[i]}
end

def host_identifier(net_addr, ip_addr)
  net_addr.each_with_index.map{|byte, i| byte ^ ip_addr[i]}
end


require "pry"

def solve s
  uniq_and_sorted_str = s.chars.uniq.sort
  
  s.length == (uniq_and_sorted_str[0]..uniq_and_sorted_str[-1]).to_a.length
end  

binding.pry

# I'd like to do something like this, but I dont manage to make it work (inject noob here)
# (number - 1).downto(1).inject(0){|sum, i|(i % 3 == 0 || i % 5 == 0) ? sum += 1 : nil}

def solution(number)
  sum = 0
  
  (number - 1).downto(1) do |i|
    if i % 3 == 0 || i % 5 == 0
      sum += i
    end
  end
  
  return sum
end


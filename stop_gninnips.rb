def spinWords(string)
  string.scan(/\w+/).map{|w| w.length >= 5 ? w.reverse : w}.join(' ')
end


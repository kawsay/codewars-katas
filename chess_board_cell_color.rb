def chessboard_cell_color(cell1, cell2)
  color(cell1) == color(cell2)
end

def color(cell)
  (cell[0].ord + cell[1].to_i) % 2
end


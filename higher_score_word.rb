require "pry"

def high(str)
  words      = str.split(" ")
  words_rank = []

  words.each do |word|
    rank = []
    word.chars.each do |c|
      rank << c.bytes.first - 96
    end
    words_rank << [word, rank.flatten.sum]
  end

  return words_rank.max_by {|x| x[1]}.first
end

high('what time are we climbing up the volcano')


# Best practices

# def high(x)
#   x.scan(/\w+/).max_by { |w| score_word(w) }
# end

# def score_word(word)
#   word.chars.inject(0) { |sum, ch| sum + (ch.ord - 96) }
# end


# def high(x)
#   x.scan(/\w+/).max_by{|x|x.sum-x.size*96}
# end


# def high(x)
#   x.split.max_by{ |word| word.sum - 96*word.length }
# end
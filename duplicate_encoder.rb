def duplicate_encode(word)
  word.downcase!
  
  word.chars.map { |c| word.count(c) > 1 ? ')' : '(' }.join
end


def palindrome(num)
  return 'Not valid' if !num.is_a?(Integer) || num < 0
  
  num = num.to_s
  
  num.chars.each_with_index.map do |c, i|  
    return true if num[i] == num[i+1]
    return true if num[i] == num[i+2]
  end
  
  return false
end


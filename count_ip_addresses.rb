def ipsBetween(start, ending)
  first_ip, last_ip = to_ip(start), to_ip(ending)
  
  p last_ip.to_i(2) - first_ip.to_i(2)
end

def to_ip(ip_str)
  ip_str.split('.').map{ |byte| byte.to_i.to_s(2).rjust(8, '0') }.join
end


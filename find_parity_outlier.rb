def find_outlier(i)
  i.count(&:odd?) > i.count(&:even?) ? i.detect(&:even?) : i.detect(&:odd?)
end


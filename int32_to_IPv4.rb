def int32_to_ip(int32)
  int32.to_s(2).rjust(32, '0').scan(/.{8}/).map { |octet| octet.to_i(2) }.join('.')
end

